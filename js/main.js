$(document).ready(() => {
    $("#register-button").hide();
    getISSPosition();

    setInterval(() => {
        getISSPosition();
    }, 2000);
    calcDistance();
});

function outlineIt() {
    var lastDist = 50000;
    $('table>tbody>tr').each(function () {
        var currDist = $(this).find('td.distance').text();
        currDist = currDist.replace('km', '')
            .replace(' ', '')
            .replace(',', '.');
        if (parseInt(lastDist) > parseInt(currDist)) {
            lastDist = currDist;
        }
    });
    $("table").find('.outlined').removeClass('outlined');
    $('.distance').each(function () {
        var currDist = $(this).text();
        currDist = currDist.replace('km', '')
            .replace(' ', '')
            .replace(',', '.');
        if (currDist === lastDist) {
            $(this).parent().addClass('outlined');
        }
    })
}

function calcDistance() {
    $('table>tbody>tr').each(function () {
        var latitude = parseFloat($(this).find('.latitude').text());
        var longitude = parseFloat($(this).find('.longitude').text());
        var issLat = $('#result-lat').text();
        var issLng = $('#result-long').text();
        $(this).find('.distance').text(parseInt(distanceInKmBetweenEarthCoordinates(latitude, longitude, issLat, issLng)) + " km");
    })
    outlineIt();
}

function degreesToRadians(degrees) {
    return degrees * Math.PI / 180;
}

function distanceInKmBetweenEarthCoordinates(lat1, lon1, lat2, lon2) {
    var earthRadiusKm = 6371;

    var dLat = degreesToRadians(lat2-lat1);
    var dLon = degreesToRadians(lon2-lon1);

    lat1 = degreesToRadians(lat1);
    lat2 = degreesToRadians(lat2);

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    return earthRadiusKm * c;
}

var oldResultLat = [

];
var oldResultLng = [

];

let getISSPosition = () => {
    $.ajax({
        url: 'http://api.open-notify.org/iss-now.json',
        context: document.body
    }).done(function (data) {
        if (data.message === "success") {
            let iss_position = data.iss_position;
            $("#result-lat").text(iss_position.latitude);
            oldResultLat.push(iss_position.latitude);
            $("#result-long").text(iss_position.longitude);
            oldResultLng.push(iss_position.longitude);
            $("#iss_history").prepend("<li>" + new Date(Date.now()).getHours() + ":" + new Date(Date.now()).getMinutes() + ":" + new Date(Date.now()).getSeconds() + " | " + iss_position.latitude + "/" + iss_position.longitude + "</li>");
        }
    });
    calcDistance();
};

$("#city").on("change keyup", function () {
    if ($(this).val().length >= 3) {
        convertAddress().then((coordinate) => {
            $("#latitude").val(coordinate.latitude);
            $("#longitude").val(coordinate.longitude);
            $("#register-button").show();
        });
    }
});

let convertAddress = () => {
    return new Promise((resolve, reject) => {

    let address = $("#city").val() + "+" + $("#country").val();

    const API_KEY = '7ba75ec0fa9840c183f4bfa8e8e15f7e';

    $.ajax({
        url: "https://api.opencagedata.com/geocode/v1/json?q="+address+"&key="+API_KEY,
        context: document.body
    }).done(function (data) {
        if(data.status.message === "OK") {
            console.log(data);
            let coordinates = {};

            let splittedData_latitude = data.results[0].annotations.DMS.lat.split(" ");
            let splittedData_longitude = data.results[0].annotations.DMS.lng.split(" ");

            let latitude_converted = parseFloat(splittedData_latitude[0].split('°')[0] + splittedData_latitude[1].split("'")[0] / 60 + splittedData_latitude[2].split("''")[0] / 3600);
            let longitude_converted = parseFloat(splittedData_longitude[0].split('°')[0] + splittedData_longitude[1].split("'")[0] / 60 + splittedData_longitude[2].split("''")[0] / 3600);

            coordinates.latitude = latitude_converted.toLocaleString(undefined,{minimumFractionDigits: 4});
            coordinates.longitude = longitude_converted.toLocaleString(undefined,{minimumFractionDigits: 4});

            console.log(coordinates);

            return resolve(coordinates);
        }
    });
    });
};