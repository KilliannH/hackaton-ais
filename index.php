<?php
session_start();
try {
    $dsn = 'mysql:dbname=equipe2;host=hackathon.ais';
    $connection = new PDO($dsn, "equipe2@localhost", "Chocolatine");
    $users = $connection->query("SELECT * FROM user");
} catch (Exception $e) {
    echo "An error occured: " . $e->getMessage() . "<br>";
    echo "<pre><code>" , var_dump($e) , "</pre></code>";
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Hackathon - Equipe 2</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script
            src="http://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
</head>
<body class="container">
<header>
    <?php include 'navbar.php';?>
</header>
    <main class="under-the-nav">
        <div class="breaking-news-iss">
            <h2>Current position of ISS</h2>
            <ul>
                <li><strong>Latitude</strong> : <span id="result-lat"></span></li>
                <li><strong>Longitude</strong> : <span id="result-long"></span></li>
            </ul>
        </div>
        <br>
        <div id="googleMap" style="width:100%;height:400px;margin-top: 25px;"></div>
        <br>
        <div class="user-table">
            <table>
                <thead>
                <tr>
                    <td>#</td>
                    <td>Mail</td>
                    <td>Lat/Lng</td>
                    <td>Distance from ISS</td>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($users as $user) {
                    echo "<tr id='" . $user['id'] . "'><td>". $user['id'] ."</td><td>" . $user['email'] . "</td><td><span class='latitude' '>" . round((float)str_replace(',', '.', $user['latitude']), 2) . "</span>/<span class='longitude'>" . round((float)str_replace(',', '.', $user['longitude']), 2) . "</span></td><td class='distance'></td></tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
        <br>
        <div>
            <h2>History</h2>
            <ul id="iss_history">

            </ul>
        </div>
        <br>

        <script>
            function myMap() {
                let ISS_latitude = $("#latitude").text();
                let ISS_longitude = $("#longitude").text();

                var mapProp= {
                    center:new google.maps.LatLng(ISS_latitude, ISS_longitude),
                    zoom:2,
                };
                var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(ISS_latitude, ISS_longitude),
                    map: map,
                    title: 'Hello World!'
                });
            }
        </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeEeKz4H-KQTFABe1UTs9h5KUlpqsZ10Q&callback=myMap"></script>
    </main>
<hr>
<footer class="page-footer font-small blue">

    <!-- Copyright -->
    <div class="flex"><img src="images/imie.png" alt="IMIE"><img src="images/ais.jpg" alt="AIS"></div>
    <!-- Copyright -->

</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="js/main.js"></script>
</body>
</html>