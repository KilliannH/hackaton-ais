<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="/~equipe2/">Hackathon <span class="ais_red">AIS</span></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav ml-auto">
            <?php
                if (isset($_SESSION['user'])) {
                    ?>
                    <li><a href="disconnect.php" class="nav-link">Disconnect</a></li>
            <?php
                } else {
                    ?>
                    <li>
                        <a class="nav-link" href="login.php">Login <span class="sr-only">(current)</span></a>
                    </li>
                    <li>
                        <a class="nav-link" href="signup.php">Sign Up <span class="sr-only">(current)</span></a>
                    </li>
            <?php
                }
            ?>
        </ul>
    </div>
</nav>