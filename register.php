<?php
session_start();
try {
    $dsn = 'mysql:dbname=equipe2;host=hackathon.ais';
    $connection = new PDO($dsn, "equipe2@localhost", "Chocolatine");
} catch (Exception $e) {
    header("Location: index.php?flash=100");
}
if (!isset($_POST) || empty($_POST)
    || empty($_POST['password-register']) || $_POST['password-register'] === ''
    || empty($_POST['email-register']) || $_POST['email-register'] === ''
    || empty($_POST['latitude']) || $_POST['latitude'] === ''
    || empty($_POST['longitude']) || $_POST['longitude'] === ''
) {
    header("Location: index.php?flash=101");
}
$query = "
                INSERT INTO 
                user (email, password, latitude, longitude) 
                VALUES ('"
    . str_replace("'", "\'", $_POST['email-register']) . "', '"
    . str_replace("'", "\'", $_POST['password-register']) . "', '"
    . str_replace("'", "\'", $_POST['latitude']) . "', '"
    . str_replace("'", "\'", $_POST['longitude']) . "')";
$state = $connection->query($query);
if ($state === false) {
    header("Location: index.php?flash=102");
 } else {
    header("Location: index.php?flash=0");
}
