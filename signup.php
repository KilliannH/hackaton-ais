<?php
session_start();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Hackathon - Equipe 2</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script
            src="http://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
</head>
<body>
<header>
    <?php include 'navbar.php';?>
</header>
<main class="under-the-nav">
    <div class="user-register">
        <h2>Sign Up</h2>
        <hr>
        <form action="register.php" class="form-register" id="register" method="post">
            <h3>About you</h3>
            <input type="hidden" value="regiter" class="form-control">
            <input type="hidden" name="latitude" id="latitude" class="form-control">
            <input type="hidden" name="longitude" id="longitude" class="form-control">
            <div class="form-group">
                <label for="email-register">E-mail</label>
                <input type="text" name="email-register" id="email-register" class="form-control">
            </div>
            <div class="form-group">
                <label for="password-register">Password</label>
                <input type="password" id="password-register" name="password-register" class="form-control">
            </div>
            <h3>About your position</h3>
            <hr>
            <div class="form-group">
                <label for="country">Country</label>
                <?php include 'countries.php'?>
            </div>
            <div class="form-group">
                <label for="city">City</label>
                <input type="text" id="city" name="city" min="0" class="form-control">
            </div>
            <br>
            <div class="text-right">
                <input type="submit" class="btn btn-red" value="Register" id="register-button" class="form-control">
            </div>
        </form>
    </div>
</main>
<hr>
<footer class="page-footer font-small blue">

    <!-- Copyright -->
    <div class="flex"><img src="images/imie.png" alt="IMIE"><img src="images/ais.jpg" alt="AIS"></div>
    <!-- Copyright -->

</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="js/main.js"></script>
</body>
</html>